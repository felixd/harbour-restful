# Harbour RESTful

Sample RESTful service for [Harbour 3.4](https://github.com/vszakats/harbour-core)

It shows how to create a RESTful server in a simple and fast way for our applications using Harbour.

## Original project

* Article: https://medium.com/harbour-magazine/restful-server-with-harbour-575e59335cf7
* Source: https://github.com/rafathefull/restful

## License

[LICENSE](LICENSE)